# Write your code here to make the tests pass.
#
class LinkedQueueNode:
    def __init__(self, value, link=None):
        self.value = value
        self.link = link

    def __str__(self):
          return f"node with values {self.value}"


class LinkedQueue:
    def __init__(self):
        self.head = None
        self.tail = None

    
    def dequeue(self):
        node = self.head
        if node.link == None:
            self.head = None
            self.tail = None
        else:
            self.head = node.link

        return node.value

    def enqueue(self, value):
        new_node = LinkedQueueNode(value)
        if self.tail == None:
            self.tail = new_node
            self.head = new_node
        else:
            self.tail.link = new_node
            self.tail = new_node



    
# Change your working directory to this directory,
# linked_queue.
# 
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_15.py.
